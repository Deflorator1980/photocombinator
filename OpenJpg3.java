package photo_combinator;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.*;

public class OpenJpg3 extends JFrame {
	JWindow wnd;
	JPanel pnl3;
	JPanel pnl, pnl2;
	ImageIcon img, img2;
	FileDialog fd, fd2;
	static int xPnl, yPnl, wPnl, hPnl, iPnl;
	static double hWpnl;
	static int xPnl2, yPnl2, wPnl2, hPnl2, iPnl2;
	static double hWpnl2;

	OpenJpg3() {
		setVisible(true);
		setLayout(null);
		setExtendedState(MAXIMIZED_BOTH);
		getContentPane().setBackground(new Color(0, 162, 232));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		fd = new FileDialog(new JFrame());
		fd.setVisible(true);
		fd2 = new FileDialog(new JFrame());
		fd2.setVisible(true);
		setExtendedState(ICONIFIED);
		setExtendedState(MAXIMIZED_BOTH);
		pnl = new Pn();
		pnl.setFocusTraversalKeysEnabled(false);
		pnl.setBounds(50, 50, 400, 400);
		pnl.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				xPnl = e.getXOnScreen();
				yPnl = e.getYOnScreen();
				pnl.requestFocus();
				setComponentZOrder(pnl, 0);
				repaint();
			}
		});
		pnl.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				pnl.setLocation(pnl.getX() + e.getXOnScreen() - xPnl,
						pnl.getY() + e.getYOnScreen() - yPnl);
				xPnl = e.getXOnScreen();
				yPnl = e.getYOnScreen();
			}
		});
		pnl.addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent e) {
				wPnl -= e.getUnitsToScroll();
				iPnl = 1;
				pnl.repaint();
			}
		});
		pnl.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					wPnl -= 5;
					iPnl = 1;
					pnl.repaint();
				}
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					wPnl += 5;
					iPnl = 1;
					pnl.repaint();
				}
				if (e.getKeyCode() == 10) {
					cucu();
				}
				if (e.getKeyCode() == 27) {
					JOptionPane.showMessageDialog(null, "Exit?");
					return;
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB) {
					pnl2.requestFocus();
					setComponentZOrder(pnl2, 0);
					pnl2.repaint();
				}
			}
		});
		add(pnl);
		pnl2 = new Pn2();
		pnl2.setFocusTraversalKeysEnabled(false);
		pnl2.setBounds(550, 50, 400, 400);
		pnl2.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				xPnl2 = e.getXOnScreen();
				yPnl2 = e.getYOnScreen();
				pnl2.requestFocus();
				setComponentZOrder(pnl2, 0);
				repaint();
			}
		});
		pnl2.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				pnl2.setLocation(pnl2.getX() + e.getXOnScreen() - xPnl2,
						pnl2.getY() + e.getYOnScreen() - yPnl2);
				xPnl2 = e.getXOnScreen();
				yPnl2 = e.getYOnScreen();
			}
		});
		pnl2.addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent e) {
				wPnl2 -= e.getUnitsToScroll();
				iPnl2 = 1;
				pnl2.repaint();
			}
		});
		pnl2.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					wPnl2 -= 5;
					iPnl2 = 1;
					pnl2.repaint();
				}
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					wPnl2 += 5;
					iPnl2 = 1;
					pnl2.repaint();
				}
				if (e.getKeyCode() == 10) {
					cucu();
				}
				if (e.getKeyCode() == 27) {
					JOptionPane.showMessageDialog(null, "Exit?");
					return;
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB) {
					pnl.requestFocus();
					setComponentZOrder(pnl, 0);
					pnl.repaint();
				}
			}
		});
		add(pnl2);
		pnl.requestFocus();
	}

	class Pn extends JPanel {
		public void paint(Graphics g) {
			super.paint(g);
			if (iPnl == 0) {
				img = new ImageIcon(fd.getDirectory() + fd.getFile());
				wPnl = img.getIconWidth();
				hPnl = img.getIconHeight();
				hWpnl = hPnl * 1.00 / wPnl;
			}
			hPnl = (int) (wPnl * hWpnl);
			pnl.setSize(wPnl, hPnl);
			BufferedImage bi = new BufferedImage(wPnl, hPnl,
					BufferedImage.TYPE_INT_RGB);
			Graphics2D g2 = (Graphics2D) bi.createGraphics();
			g2.addRenderingHints(new RenderingHints(
					RenderingHints.KEY_RENDERING,
					RenderingHints.VALUE_RENDER_QUALITY));
			g2.drawImage(img.getImage(), 0, 0, wPnl, hPnl, null);
			g.drawImage(bi, 0, 0, wPnl, hPnl, this);
		}
	}

	class Pn2 extends JPanel {
		public void paint(Graphics g) {
			super.paint(g);
			if (iPnl2 == 0) {
				img2 = new ImageIcon(fd2.getDirectory() + fd2.getFile());
				wPnl2 = img2.getIconWidth();
				hPnl2 = img2.getIconHeight();
				hWpnl2 = hPnl2 * 1.00 / wPnl2;
			}
			hPnl2 = (int) (wPnl2 * hWpnl2);
			pnl2.setSize(wPnl2, hPnl2);
			BufferedImage bi = new BufferedImage(wPnl2, hPnl2,
					BufferedImage.TYPE_INT_RGB);
			Graphics2D g2 = (Graphics2D) bi.createGraphics();
			g2.addRenderingHints(new RenderingHints(
					RenderingHints.KEY_RENDERING,
					RenderingHints.VALUE_RENDER_QUALITY));
			g2.drawImage(img2.getImage(), 0, 0, wPnl2, hPnl2, null);
			g.drawImage(bi, 0, 0, wPnl2, hPnl2, this);
		}
	}

	public void cucu() {
		wnd = new Wn();
		wnd.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				pnl3.setLocation(e.getXOnScreen(), e.getYOnScreen());
			}

			public void mouseReleased(MouseEvent e) {
				Rectangle hui = new Rectangle(pnl3.getBounds());
				hui.x += 1;
				hui.y += 1;
				hui.width -= 2;
				hui.height -= 2;
				try {
					BufferedImage img3 = new Robot().createScreenCapture(hui);
					FileDialog fd3 = new FileDialog(new JFrame(), "Saving",
							FileDialog.SAVE);
					fd3.setDirectory("c:\\Video\\Porn");
					fd3.setFile("*.jpg");
					fd3.setVisible(true);
					try {
						ImageIO.write(img3, "jpg", new File(fd3.getDirectory()
								+ fd3.getFile()));
					} catch (IOException ex) {
					}
				} catch (AWTException ex) {
				}
				return;
			}
		});
		wnd.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				pnl3.setSize(e.getXOnScreen() - pnl3.getX(), e.getYOnScreen()
						- pnl3.getY());
			}
		});
	}

	class Wn extends JWindow {
		Wn() {
			setLayout(null);
			setVisible(true);
			setSize(getToolkit().getScreenSize());
			setBackground(new Color(00, 00, 0, 1));
			setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
			pnl3 = new JPanel();
			pnl3.setBounds(0, 0, 00, 00);
			pnl3.setBorder(BorderFactory.createLineBorder(Color.red));
			pnl3.setBackground(new Color(0, 0, 0, 0));
			add(pnl3);
		}
	}

	public static void main(String[] args) {
		new OpenJpg3();
	}
}
